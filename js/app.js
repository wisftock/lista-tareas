const listaTareas = document.querySelector('.listaTarea');
const formulario = document.querySelector('#formulario');
eventListener()
function eventListener(){
	formulario.addEventListener('submit', agregarTarea);
	listaTareas.addEventListener('click', borrarTarea);
}

function agregarTarea(e){
	e.preventDefault();

	const nombre = document.querySelector('.nombre').value;
	const tareaTexarea = document.querySelector('.tarea').value;

	// creando elemento
	const li = document.createElement('li');
	li.textContent = `${nombre} -  ${tareaTexarea}`

	const i = document.createElement('i');
	i.innerText = 'X';
	i.className = 'eliminar';

	li.appendChild(i);

	listaTareas.appendChild(li);
	formulario.reset();
}

function borrarTarea(e){
	if (e.target.className === 'eliminar') 
		e.target.parentElement.remove();
	
}